## What is it

It's an example of usage [docker nginx proxy](https://hub.docker.com/r/jwilder/nginx-proxy) with [letsencrypt companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion) and [nginx-cf-real-ip](https://hub.docker.com/r/mikenowak/nginx-cf-real-ip).  
It listens ports (80,443) and proxying traffic to the specific container dependent on a requested server name.  
It solves a port owner and proxying problems, when a docker host have several website containers with different domains.  
It has [letsencrypt companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion), so will be auto create and renew ssl certificates for using domains.  
It has a special config for CloudFlare, to restore real ips. See volume/nginx-proxy/conf/cloudflare.conf. Must be updated regulary from [the official page](https://www.cloudflare.com/ips/).

## How to use

After running all containers connected to the `nginx-proxy_proxy` network with `VIRTUAL_HOST` and `LETSENCRYPT_HOST`
environment variables will receive target traffic and domains will have ssl certificates.  
The target ports (80,443) should be exposed on these containers.  
See more information about settings on the packages pages: [docker nginx proxy](https://hub.docker.com/r/jwilder/nginx-proxy), [letsencrypt companion](https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion)

## Management

Create and run containers: `docker-compose up -d`  
Stop and remove containers: `docker-compose down`  
Http authentication for a host: `htpasswd -c ./volume/nginx-proxy/htpasswd/{domain} {username}`

## Example of usage
See [example of usage](https://gitlab.com/lightsource/docker-website) 
